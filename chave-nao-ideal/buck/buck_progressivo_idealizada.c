double IL_act = 0;
double Vc_act = 0;
static double IL_ant = 0;
static double Vc_ant = 0;
static double Vs = 0;
static double Is = 0;

static int prev_state = 1;
static double count = 0;
static double t_on = 0.000001;

#define Vi 12
#define R 2
#define L (100*pow(10, -6))
#define C (200*pow(10, -6))
#define Vs_min 1.2

if(x1 != prev_state){
	count = count + delt;
	if(prev_state == 0 && count <= t_on){
		Vs = (Vi*t_on + (Vs_min-Vi)*count )/t_on;
		Is = IL_ant*count/t_on;
	}
	if(prev_state == 1 && count <= t_on){
		Vs = (Vs_min*t_on + (Vi-Vs_min)*count)/t_on;
		Is = (IL_ant*t_on - IL_ant*count)/t_on;
	}
	if(count>t_on){
		prev_state = x1;
		count = 0;
	}

//y = (xb*ya - xa*yb + (yb - ya)x)/(xb - xa)	
}
else if (x1 == 1){
	Vs = Vs_min;
	Is = IL_ant;
}
else{
	Vs = Vi;
	Is = 0;
}

IL_act = IL_ant + (delt/L)*((Vi-Vs) - Vc_ant);
Vc_act = Vc_ant + (delt/C)*(IL_ant - Vc_ant/R);

if( IL_act <= 0){
	IL_act = 0;
	Vc_act = Vc_ant - (delt*Vc_ant/(C*R));
}

IL_ant = IL_act;
Vc_ant = Vc_act;

y1 = IL_act;
y2 = Vc_act;
y3 = Is;
y4 = Vs;
y5 = IL_act-Is;
y6 = Vi-Vs;