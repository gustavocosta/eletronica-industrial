double IL1_act = 0;
double IL2_act = 0;
double Vc1_act = 0;
double Vc2_act = 0;
static double IL1_ant = 0;
static double IL2_ant = 0;
static double Vc1_ant = 0;
static double Vc2_ant = 0;
static double Vs = 0;
static double Is = 0;

static int prev_state = 1;
static double count = 0;
static double t_on = 0.000001;

#define Vi 12
#define R 3.57
#define L1 (150*pow(10, -6))
#define L2 (200*pow(10, -6))
#define C1 (200*pow(10, -6))
#define C2 (220*pow(10, -6))
#define Vs_min -1.2

if(x1 != prev_state){
	count = count + delt;
	if(prev_state == 0 && count <= t_on){
		Vs = (Vc1_ant*t_on + (Vs_min-Vc1_ant)*count )/t_on;
		Is = (IL1_ant + IL2_ant)*count/t_on;
	}
	if(prev_state == 1 && count <= t_on){
		Vs = (Vs_min*t_on + ( Vc1_ant - Vs_min)*count)/t_on;
		Is = ((IL1_ant + IL2_ant)*t_on - (IL1_ant + IL2_ant)*count)/t_on;
	}
	if(count>t_on){
		prev_state = x1;
		count = 0;
	}

//y = (xb*ya - xa*yb + (yb - ya)x)/(xb - xa)	
}
else if (x1 == 1){
	Vs = Vs_min;
	Is = IL1_ant + IL2_ant;
}
else{
	Vs = Vc1_ant;
	Is = 0;
}

IL1_act = IL1_ant + (delt/L1)*(Vi + Vs);
IL2_act = IL2_ant - (delt/L2)*((Vc1_ant - Vs) + Vc2_ant);

Vc1_act = Vc1_ant + (delt/C1)*(Is - IL1_ant);
Vc2_act = Vc2_ant + (delt/C2)*(IL2_ant - (Vc2_ant/R));

if(x1 == 0 && IL1_act+IL2_act < 0.01){
	Vc1_act = Vc1_ant + (delt/C1)*IL2_ant;

	IL1_act =  IL1_ant + (delt/(L1+L2))*(Vi + Vc1_ant + Vc2_ant);
	IL2_act = -IL1_act;
}

IL1_ant = IL1_act;
IL2_ant = IL2_act;
Vc1_ant = Vc1_act;
Vc2_ant = Vc2_act;

y1 = IL1_act;
y2 = IL2_act;
y3 = Vc1_act;
y4 = Vc2_act;
y5 = Is;
y6 = Vs;
y7 = IL1_act + IL2_ant - Is;
y8 = Vc1_act - Vs;