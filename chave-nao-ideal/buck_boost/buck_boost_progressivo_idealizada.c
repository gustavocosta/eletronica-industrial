double IL_act = 0;
double Vc_act = 0;
static double IL_ant = 0;
static double Vc_ant = 0;
static double Vs = 0;
static double Is = 0;

static int prev_state = 1;
static double count = 0;
static double t_on = 0.000001;

#define Vi 12
#define R 12
#define L (250*pow(10, -6))
#define C (220*pow(10, -6))
#define Vs_min 1.2

if(x1 != prev_state){
	count = count + delt;
	if(prev_state == 0 && count <= t_on){
		Vs = ((Vi + Vc_ant)*t_on + (Vs_min-(Vi + Vc_ant))*count )/t_on;
		Is = IL_ant*count/t_on;
	}
	if(prev_state == 1 && count <= t_on){
		Vs = (Vs_min*t_on + ((Vi + Vc_ant)-Vs_min)*count)/t_on;
		Is = (IL_ant*t_on - IL_ant*count)/t_on;
	}
	if(count>t_on){
		prev_state = x1;
		count = 0;
	}

//y = (xb*ya - xa*yb + (yb - ya)x)/(xb - xa)	
}
else if (x1 == 1){
	Vs = Vs_min;
	Is = IL_ant;
}
else{
	Vs = Vi + Vc_ant;
	Is = 0;
}

IL_act = IL_ant + (delt/L)*(Vi - Vs);
Vc_act = Vc_ant + (delt/C)*((IL_ant - Is) - Vc_ant/R);

if(x1 == 0 && IL_act <= 0){
	IL_act = 0;
	Vc_act = Vc_ant - (delt/(C*R))* Vc_ant;
}

IL_ant = IL_act;
Vc_ant = Vc_act;

y1 = IL_act;
y2 = Vc_act;
y3 = Is;
y4 = Vs;
y5 = IL_act-Is;
y6 = Vi+Vc_act-Vs;