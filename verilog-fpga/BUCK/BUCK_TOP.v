module BUCK_TOP (
	//////// CLOCK //////////
	CLOCK_50,
	
	//////// LED //////////
	LEDR,
	
	//////// KEY //////////
	KEY,

	//////// SW //////////
	SW,

	//////// SEG7 //////////
	HEX0,
	HEX1,
	HEX2,
	HEX3,
	HEX4,
	HEX5
	
);

	//////////// CLOCK //////////
	input						CLOCK_50;
	
	//////////// LED //////////
	output	   [9:0]	   LEDR;

	//////////// KEY //////////
	input		   [3:0]		KEY;

	//////////// SW //////////
	input			[9:0]		SW;

	//////////// SEG7 //////////
	output		[6:0]		HEX0;
	output		[6:0]		HEX1;
	output		[6:0]		HEX2;
	output		[6:0]		HEX3;
	output		[6:0]		HEX4;
	output		[6:0]		HEX5;
	
	//////////// Wires //////////
	wire						CLK;
	wire						PWM;
	wire			[31:0]	MUX_OUT;
	wire			[23:0]	CONV_OUT;
	
	wire			[31:0]	IL;
	wire			[31:0]	Vc;
	
	wire			[31:0]	DELT;
	wire			[31:0]	P;
	wire			[31:0]	DUTY;
	
	wire			[31:0]	slow;
	
	assign DELT = 418;		// 0.0000001(100ns) * 1000
	assign P		= 41942;		// 0.00001(10us) * 1000
	assign DUTY = 2097152;	// 0.5
	
	assign slow = SW[9] ? 32'd419430400 : 32'd4194304; // 1s or 0.01s
	
	PWM_GENERATOR	pwm1	(.CLK(CLOCK_50), .RST(KEY[1]), .DELT(8), .DUTY(2097152), .P(slow), .PWM(CLK));
	PWM_GENERATOR	pwm2	(.CLK(CLK), .RST(KEY[0]), .DELT(DELT), .DUTY(DUTY), .P(P), .PWM(PWM));
//	PWM_GENERATOR	pwm2	(.CLK(CLOCK_50), .RST(KEY[0]), .DELT(DELT), .DUTY(DUTY), .P(P), .PWM(PWM));

	assign LEDR[9] = CLOCK_50;
	assign LEDR[8] = CLK;
	assign LEDR[7] = PWM;
	
	BUCK_TUSTIN		bt1	(.CLK(CLK), .RST(KEY[0]), .PWM(PWM), .DELT(DELT), .Vc(Vc), .IL(IL), .test(test));
	
	MUX4X1			mux1	(.In0(IL), .In1(Vc), .In2(0), .In3(0), .Sel(SW[1:0]), .Out(MUX_OUT));
	
	B32to24			b1		(.In(MUX_OUT), .Out(CONV_OUT));
//	assign LEDR[6:0] = CONV_OUT[6:0];
	
	SEG7_LUT	u0	(.oSEG(HEX0),.iDIG(CONV_OUT[3:0]));
	SEG7_LUT	u1	(.oSEG(HEX1),.iDIG(CONV_OUT[7:4]));
	SEG7_LUT	u2	(.oSEG(HEX2),.iDIG(CONV_OUT[11:8]));
	SEG7_LUT	u3	(.oSEG(HEX3),.iDIG(CONV_OUT[15:12]));
	SEG7_LUT	u4	(.oSEG(HEX4),.iDIG(CONV_OUT[19:16]));
	SEG7_LUT	u5	(.oSEG(HEX5),.iDIG(CONV_OUT[23:20]));
endmodule
