module BUCK_TUSTIN (
	CLK,
	RST,
	PWM,
	DELT,
	Vc,
	IL,
	test
);

	input 					CLK;
	input 					RST;
	input						PWM;
	input			[31:0]	DELT;
	output reg	[31:0]	Vc;
	output reg	[31:0]	IL;
	output reg	[31:0]	test;
	
	
	//// Constants Declaration
	wire			[31:0]	Vi, R, L, C, rep_2;
	wire			[7:0]		t_on;
	
	//// Constants Atribution
	assign Vi		= 32'd50331648;	// Qf 12
	assign R			= 32'd8388608;	// Qf 2
	assign L			= 32'd419430;	// Qf 0.0001 * 1000
	assign C 		= 32'd838860;	// Qf 0.0002 * 1000
	assign rep_2	= 32'd8388608;	// Qf 2
	assign t_on		= 32'd249;		// D 249
	
	//// Variables
	reg			[31:0]	IL_forward, Vc_forward, IL_ant, Vc_ant, Vs, Is;
	reg			[7:0]		count;
	reg						prev_state;
	
	//// Auxiliar Variables
	reg			[31:0]	A;
	reg			[31:0]	aux1, aux2, aux3, aux4;
	reg			[63:0]	aux64;
	
	always @ (negedge CLK) begin
		if (~RST) begin
			IL_forward = 0;
			Vc_forward = 0;
			IL_ant = 32'd0;
			Vc_ant = 32'd0;
//			IL_ant = 32'd13174277;
//			Vc_ant = 32'd25166230;
			Is = 0;
			Vs = 0;
			
			count = 0;
			prev_state = 0;
			
			A = 0;
			aux1 = 0;
			aux2 = 0;
			aux3 = 0;
			aux4 = 0;
		end

//		(A*B)>>>22;

//		A = {32'd0, A[31] ? -A : A};
//		(A<<<22)/B;
//		A[31] ? -Vc : Vc

		// Vc_forward
		aux64 = (DELT<<<22)/C;
		aux1 = aux64;
		
		A = Vc_ant[31] ? -Vc_ant : Vc_ant;
		aux64 = (A<<<22)/R;
		aux2 = Vc_ant[31] ? -aux64 : aux64;
		
		aux64 = (aux1 * (IL_ant-aux2));
		aux3 = aux64 >>> 22;
		
		Vc_forward = Vc_ant + aux3;
		
		// IL
		aux64 = rep_2 * L;
		aux1 = aux64 >>> 22;
		
		aux64 = (DELT<<<22)/aux1;
		aux2 = aux64;
		
		aux64 = aux2 * ((PWM?Vi:0) - Vc_forward + (PWM?Vi:0) - Vc_ant);
		aux3 = aux64 >>> 22;
		
		IL = IL_ant + aux3;
		
		// Vc
		aux64 = (rep_2 * C);
		aux1 = aux64>>>22;
		
		aux64 = (DELT<<<22)/aux1;
		aux2 = aux64;
		
		A = Vc_forward[31] ? -Vc_forward : Vc_forward;
		aux64 = (A<<<22)/R;
		aux3 = Vc_forward[31] ? -aux64 : aux64;
		
		A = Vc_ant[31] ? -Vc_ant : Vc_ant;
		aux64 = (A<<<22)/R;
		aux4 = Vc_ant[31] ? -aux64 : aux64;
		test = aux4;
		
		aux64 = (aux2 * (IL - aux3 + IL_ant - aux4));
		aux1 = aux64>>>22;
		
		Vc = Vc_ant + aux1;
		
		if (IL[31] == 1 || IL == 32'd0) begin
			// Vc_forward
			aux64 = (DELT<<<22)/C;
			aux1 = aux64;
			
			A = Vc_ant[31] ? -Vc_ant : Vc_ant;
			aux64 = (A<<<22)/R;
			aux2 = Vc_ant[31] ? -aux64 : aux64;
			
			aux64 = (aux1 * aux2);
			aux3 = aux64 >>> 22;
			
			Vc_forward = Vc_ant - aux3;
			
			// IL
			IL = 32'd0;
			
			// Vc	
			aux64 = (C * R);
			aux1 = aux64>>>22;
			
			aux64 = (rep_2 * aux1);
			aux2 = aux64>>>22;
			
			aux64 = (DELT<<<22)/aux2;
			aux3 = aux64;
			
			aux64 = (aux3 * (Vc_forward + Vc_ant));
			aux4 = aux64>>>22;
			
			Vc = Vc_ant + aux4;
		end
		
		IL_ant = IL;
		Vc_ant = Vc;
	end

endmodule