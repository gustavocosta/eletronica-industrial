module DIV (A, B, OUT);

	input			[31:0] A, B;
	output reg	[31:0] OUT;
	
	reg [63:0] A_, B_;
	reg isNeg;
	reg [63:0] temp;
	reg [31:0] div;
	
	always @ (A or B) begin
		isNeg = 0;
		
//		if(A[31] == 1) begin
//			isNeg = ~isNeg;
//			A_ = -A;
//		end
//		else begin
//			A_ = A;
//		end
//		
//		if(B[31] == 1) begin
//			isNeg = ~isNeg;
//			B_ = -B;
//		end
//		else begin
//			B_ = B;
//		end
		A_ = A;
		B_ = B;
		
		temp = A_ << 22;
		
		if ((temp >= 0 && B_ >= 0) || (temp < 0 && B_ < 0)) begin
			temp = temp + (B_ >> 1);
		end
		else begin
			temp = temp - (B_ >> 1);
		end
		
		div = temp/B_;
		
//		if(isNeg) begin
//			div = -div;
//		end
		
		OUT = div;
		
end

endmodule
