module MUX4X1(In0, In1, In2, In3, Sel, Out);

	input [31:0] In0, In1, In2, In3;
	input [1:0] Sel;
	
	output reg [31:0] Out;
	
	always @ (Sel or In0 or In1 or In2 or In3)
	begin
		case(Sel)
			2'b00: Out = In0;
			2'b01: Out = In1;
			2'b10: Out = In2;
			2'b11: Out = In3;
		endcase
	end

endmodule
