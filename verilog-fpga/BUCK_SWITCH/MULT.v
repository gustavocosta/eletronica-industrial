module MULT (A, B, OUT);

	input			[31:0] A;
	input			[31:0] B;
	output reg	[31:0] OUT;
	
	reg [31:0] A_, B_;
	reg isNeg;
	reg [63:0] multhi;
	reg [31:0] multhi_;
	
	
	always @ (A or B) begin
		isNeg = 0;
		
		if(A[31] == 1) begin
			isNeg = ~isNeg;
			A_ = -A;
		end
		else begin
			A_ = A;
		end
		
		if(B[31] == 1) begin
			isNeg = ~isNeg;
			B_ = -B;
		end
		else begin
			B_ = B;
		end
		
		multhi = A_*B_;
		
		multhi_ = multhi >> 22;
		
		if(isNeg) begin
			multhi_ = -multhi_;
		end
		
		OUT = multhi_;
		
	end
		
endmodule
