module PWM_GENERATOR(CLK, RST, DELT, DUTY, P, PWM);
	
	input				CLK;
	input				RST;
	input [31:0]	DUTY;
	input [31:0]	P;
	input [31:0]	DELT;
	
	output reg PWM;
	
	reg	[31:0]	count;
	reg	[31:0]	P_prev;
	reg	[31:0]	P_up;
	reg	[31:0]	P_down;
	reg	[63:0]	aux64;
	
	always @ (posedge CLK) begin
		
		if (!RST) begin
			count = 0;
			P_prev = P;
		end
		
		if (!P_prev) begin
			P_prev = P;
		end
		
		if (P_prev - count <= DELT) begin
			count = 0;
			P_prev = P;
		end
		
		count = count + DELT;
		
		aux64 = P_prev * DUTY;
		P_up = aux64 >>> 22;
		P_down = P_prev - P_up;
		if (count > P_down) begin
			PWM = 1;
		end
		else begin
			PWM = 0;
		end
	end

endmodule
