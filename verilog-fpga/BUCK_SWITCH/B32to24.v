module B32to24(In, Out);

	input			[31:0] In;
	output		[23:0] Out;
	
	assign Out = In[31] ? 32'd0 : In >> 7; 

endmodule
