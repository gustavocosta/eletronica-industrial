double IL_act = 0;
double Vc_act = 0;
static double IL_ant = 0;
static double Vc_ant = 0;

#define Vi 12
#define R 12
#define L (250*pow(10, -6))
#define C (220*pow(10, -6))

double Vc_forward = Vc_ant + (delt/C)*(IL_ant*(1-x1) - Vc_ant/R);

IL_act = IL_ant + (delt/L)*(Vi*x1 - Vc_forward*(1-x1));
Vc_act = Vc_ant + (delt/C)*(IL_act*(1-x1) - Vc_forward/R);

if(x1 == 0 && IL_act <= 0){
	Vc_forward = Vc_ant - (delt/(C*R))* Vc_ant;	

	IL_act = 0;
	Vc_act = Vc_ant - (delt/(C*R))* Vc_forward;
}

IL_ant = IL_act;
Vc_ant = Vc_act;

y1 = IL_act;
y2 = Vc_act;