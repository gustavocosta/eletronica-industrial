double IL1_act = 0;
double IL2_act = 0;
double Vc1_act = 0;
double Vc2_act = 0;
static double IL1_ant = 0;
static double IL2_ant = 0;
static double Vc1_ant = 0;
static double Vc2_ant = 0;

#define Vi 12
#define R 3.57
#define L1 (150*pow(10, -6))
#define L2 (200*pow(10, -6))
#define C1 (200*pow(10, -6))
#define C2 (220*pow(10, -6))

double Vc1_forward = Vc1_ant + (delt/C1)*( IL2_ant*x1 - IL1_ant*(1-x1));
double Vc2_forward = Vc2_ant + (delt/C2)*(IL2_ant - (Vc2_ant/R));

IL1_act = IL1_ant + (delt/(2*L1))*(2*Vi + (Vc1_forward + Vc1_ant)*(1-x1));
IL2_act = IL2_ant - (delt/(2*L2))*( (Vc1_forward + Vc1_ant)*x1 + Vc2_forward + Vc2_ant);

Vc1_act = Vc1_ant + (delt/(2*C1))*((IL2_act + IL2_ant)*x1 - (IL1_act + IL1_ant)*(1-x1));
Vc2_act = Vc2_ant + (delt/(2*C2))*(IL2_act - (Vc2_forward/R) + IL2_ant - (Vc2_ant/R));

if(x1 == 0 && IL1_act+IL2_act < 0.01){
	Vc1_forward = Vc1_ant + (delt/C1)*IL2_ant;

	IL1_act =  IL1_ant + (delt/(2*(L1+L2)))*(2*Vi + Vc1_forward + Vc2_forward + Vc1_ant + Vc2_ant);
	IL2_act = -IL1_act;

	Vc1_act = Vc1_ant + (delt/(2*C1))*(IL2_act + IL2_ant);
	Vc2_act =  Vc2_ant + (delt/(2*C2))*(IL2_act - (Vc2_forward/R) + IL2_ant - (Vc2_ant/R));
}

IL1_ant = IL1_act;
IL2_ant = IL2_act;
Vc1_ant = Vc1_act;
Vc2_ant = Vc2_act;

y1 = IL1_act;
y2 = IL2_act;
y3 = Vc1_act;
y4 = Vc2_act;
