double IL_act = 0;
double Vc_act = 0;
static double IL_ant = 0;
static double Vc_ant = 0;

#define Vi 12
#define R 2
#define L (100*pow(10, -6))
#define C (200*pow(10, -6))

double Vc_forward = Vc_ant + (delt/C)*(IL_ant - Vc_ant/R);

IL_act = IL_ant + (delt/(2*L))*((Vi*x1) - Vc_forward + (Vi*x1) - Vc_ant);
Vc_act = Vc_ant + (delt/(2*C))*(IL_act - (Vc_forward/R) + IL_ant - (Vc_ant/R));

if(x1 == 0 && IL_act <=0 ){
	Vc_forward = Vc_ant - (delt*Vc_ant/(C*R));

	IL_act = 0;
	Vc_act = Vc_ant - (delt/(2*C*R))*(Vc_forward + Vc_ant);
}


IL_ant = IL_act;
Vc_ant = Vc_act;

y1 = IL_act;
y2 = Vc_act;